<?php

namespace App\Tests\Controller;

use App\Controller\HomeController;
use App\Entity\ReservationDetails;
use App\Entity\RoomInfo;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HomeControllerTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * testing initial execution
     */
    public function testInitialExecution(): void
    {
        $this->assertTrue(true);
    }

    /**
     * testing findFullyReservedDays method
     */
    public function testFindFullyReservedDays():void{
        $controller = new HomeController();
        $totalReserved = count($controller->findFullyReservedDays($this->entityManager
            ->getRepository(RoomInfo::class)));

        $this->assertSame(0, $totalReserved );
    }

    /**
     * testing findDayWiseReservationDetails method
     */
    public function testFindDayWiseReservationDetails():void{
        $controller = new HomeController();
        $totalReserved = count($controller->findDayWiseReservationDetails($this->entityManager
            ->getRepository(ReservationDetails::class)));

        $this->assertSame(3, $totalReserved );
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

}
