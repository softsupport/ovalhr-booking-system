<?php

namespace App\Form;

use App\Entity\ReservationDetails;
use App\Repository\RoomInfoRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ReservationType extends AbstractType
{
    private RoomInfoRepository $roomInfoRepository;

    public function __construct(RoomInfoRepository $roomInfoRepository)
    {
        $this->roomInfoRepository = $roomInfoRepository;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class,[
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'error_bubbling'=> true,
                'constraints' =>[
                    new Callback([$this, 'validateNotPastDate'], null, 'Start Date')
                ]
            ])
            ->add('endDate', DateType::class,[
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'error_bubbling'=> true,
                'constraints' =>[
                    new Callback([$this, 'validateNotPastDate'], null, 'End Date'),
                    new Callback([$this, 'validateDateSelection']),
                    new Callback([$this, 'validateVacancy'])
                ]
            ])
        ;
    }

    public function validateNotPastDate($value, ExecutionContextInterface $context, $payload){
        $data = $context->getRoot()->getData();

        $totalDays = (date_diff($data->getStartDate(), \DateTime::createFromFormat('Y-m-d', date('Y-m-d'))));
        if(!$totalDays->invert){
            $context
                ->buildViolation($payload." should be larger than today")
                ->addViolation();
        }
    }

    public function validateDateSelection($value, ExecutionContextInterface $context){
        $data = $context->getRoot()->getData();

        $totalDays = (date_diff($data->getStartDate(), $data->getEndDate()));

        if($totalDays->invert){
            $context
                ->buildViolation("Start date can't be larger than end date, please check your dates.")
                ->addViolation();
        }
    }

    public function validateVacancy($value, ExecutionContextInterface $context){
        $data = $context->getRoot()->getData();
        $noVacancy = false;

        $totalDays = (date_diff($data->getStartDate(), $data->getEndDate()));
        $startDate = $data->getStartDate()->format('Y-m-d');

        for($i = 0; $i < $totalDays->days+1; $i++){
            $currentDate = date("Y-m-d", strtotime("$startDate +$i day"));

            $currentDateDetails = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date($currentDate))]);
            if($currentDateDetails && $currentDateDetails->getExistingAmount() < 1){
                $noVacancy=true;
                $problematicDate = $currentDate;
                break;
            }
        }

        if($noVacancy){
            $context
                ->buildViolation("No vacancy availavle for ".$problematicDate)
                ->addViolation();
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReservationDetails::class,
        ]);
    }
}
