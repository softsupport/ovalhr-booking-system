<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerApTest extends WebTestCase
{
    /**
     * Test Home page
     */
    public function testHomeIsShowingData(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Welcome to Room Reservation');
    }

    /**
     * Test successful form submission using dummy data
     */
    public function testFormSubmission():void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        // select the button
        $buttonCrawlerNode = $crawler->selectButton('Reserve');

        // retrieve the Form object for the form belonging to this button
        $form = $buttonCrawlerNode->form();

        // set values on a form object
        $form['reservation[startDate]'] = date('Y-m-d', strtotime('2021-07-20'));
        $form['reservation[endDate]'] = date('Y-m-d', strtotime('2021-07-20'));

        // submit the Form object
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    /**
     * Test successful form submission using wrong data
     */
    public function testFormSubmissionWithWrongEntry():void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        // select the button
        $buttonCrawlerNode = $crawler->selectButton('Reserve');

        // retrieve the Form object for the form belonging to this button
        $form = $buttonCrawlerNode->form();

        // set values on a form object
        $form['reservation[startDate]'] = '';
        $form['reservation[endDate]'] = date('Y-m-d', strtotime('2021-07-20'));

        // submit the Form object
        $client->submit($form);
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }
}
