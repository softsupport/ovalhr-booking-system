<?php

namespace App\Repository;

use App\Entity\RoomInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoomInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomInfo[]    findAll()
 * @method RoomInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoomInfo::class);
    }

    // /**
    //  * @return RoomInfo[] Returns an array of RoomInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoomInfo
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
