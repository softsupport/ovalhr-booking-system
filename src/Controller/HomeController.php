<?php

namespace App\Controller;

use App\Entity\ReservationDetails;
use App\Entity\RoomInfo;
use App\Form\ReservationType;
use App\Repository\ReservationDetailsRepository;
use App\Repository\RoomInfoRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request
        , EntityManagerInterface $em
        , RoomInfoRepository $roomInfoRepository
        , ReservationDetailsRepository $reservationDetailsRepository
    ): Response
    {
        $reservationForm = $this->createForm(ReservationType::class);
        $reservationForm->handleRequest($request);

        if($reservationForm->isSubmitted() && $reservationForm->isValid()){

            $em->getConnection()->beginTransaction();

            try {
                $totalPrice = 0;

                /** @var ReservationDetails $reservationData */
                $reservationData = $reservationForm->getData();
                $reservationData->setCreatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
                $reservationData->setUpdatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));

                $em->persist($reservationData);
                $em->flush();

                //update room capacity & total for each date
                $totalDays = (date_diff($reservationData->getStartDate(), $reservationData->getEndDate()));
                $startDate = $reservationData->getStartDate()->format('Y-m-d');

                for($i = 0; $i < $totalDays->days+1; $i++){

                    $currentDate = date("Y-m-d", strtotime("$startDate +$i day"));
                    $roomData = $roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date($currentDate))]);

                    //calculate total price
                    $totalPrice += $roomData->getPrice();

                    //update vacancy data for each date
                    $roomData->setExistingAmount($roomData->getExistingAmount() -1);
                    $em->persist($roomData);
                    $em->flush();
                }

                $em->getConnection()->commit();

                $this->addFlash('total_reservation_price', $totalPrice);
                return $this->redirect($request->getUri());
            } catch (Exception $e) {
                $em->getConnection()->rollBack();
                throw $e;
            }

        }

        //find existing booking data & prepare events array
        //$reservations = $this->findDayWiseReservationDetails($reservationDetailsRepository);

        //find vacant or reserved status for selected dates
        $reservations = $this->findFullyReservedDays($roomInfoRepository);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'reservationForm' => $reservationForm->createView(),
            'reservations' => $reservations
        ]);
    }

    /**
     * Find fully reserved days
     * @param RoomInfoRepository $roomInfoRepository
     * @return array
     */
    public function findFullyReservedDays(RoomInfoRepository $roomInfoRepository):array{
        $existingReservationList = $roomInfoRepository->findBy(['existingAmount' => 0]);
        $totalReservation = count($existingReservationList);

        $reservations =[];

        for($j = 0; $j < $totalReservation; $j++){

            $currentReservation = [
                'title' => 'Fully Reserved',
                'start' => $existingReservationList[$j]->getDateDetails()->format('Y-m-d').'T00:00:00',
                'end' => $existingReservationList[$j]->getDateDetails()->format('Y-m-d').'T23:00:00'
            ];

            array_push($reservations, $currentReservation);
        }

        return $reservations;
    }

    /**
     * Find reservation details data
     * @param ReservationDetailsRepository $reservationDetailsRepository
     * @return array
     */
    public function findDayWiseReservationDetails(ReservationDetailsRepository $reservationDetailsRepository):array{
        $existingReservationList = $reservationDetailsRepository->findAll();
        $totalReservation = count($existingReservationList);

        $reservations =[];

        for($j = 0; $j < $totalReservation; $j++){

            if(date_diff($existingReservationList[$j]->getStartDate(),$existingReservationList[$j]->getEndDate())->days == 0){
                $title = "Reservation ".($j+1).' for '.$existingReservationList[$j]->getStartDate()->format('d');
            }
            else{
                $title = "Reservation ".($j+1).' for '.$existingReservationList[$j]->getStartDate()->format('d').' to '. $existingReservationList[$j]->getEndDate()->format('d');
            }

            $currentReservation = [
                'title' => $title,
                'start' => $existingReservationList[$j]->getStartDate()->format('Y-m-d').'T00:00:00',
                'end' => $existingReservationList[$j]->getEndDate()->format('Y-m-d').'T23:00:00'
            ];

            array_push($reservations, $currentReservation);
        }

        return $reservations;
    }
}
