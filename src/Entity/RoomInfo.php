<?php

namespace App\Entity;

use App\Repository\RoomInfoRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=RoomInfoRepository::class)
 */
class RoomInfo
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDetails;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $existingAmount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDetails(): ?\DateTimeInterface
    {
        return $this->dateDetails;
    }

    public function setDateDetails(\DateTimeInterface $dateDetails): self
    {
        $this->dateDetails = $dateDetails;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getExistingAmount(): ?int
    {
        return $this->existingAmount;
    }

    public function setExistingAmount(?int $existingAmount): self
    {
        $this->existingAmount = $existingAmount;

        return $this;
    }
}
