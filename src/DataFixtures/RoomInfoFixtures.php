<?php

namespace App\DataFixtures;

use App\Entity\ReservationDetails;
use App\Entity\RoomInfo;
use App\Repository\RoomInfoRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoomInfoFixtures extends Fixture
{
    private RoomInfoRepository $roomInfoRepository;

    public function __construct(RoomInfoRepository $roomInfoRepository)
    {

        $this->roomInfoRepository = $roomInfoRepository;
    }

    public function load(ObjectManager $manager)
    {


        $months = [31,28,31,30,31,30,31,31,30,31,30,31];
        for($i = 0; $i < 12; $i++){
            for($j=0; $j<$months[$i]; $j++){

                $month = $i+1;
                $date = $j+1;

                if($month<10){
                    $month = '0'.$month;
                }

                if($date<10){
                    $date = '0'.$date;
                }

                $roomInfo = new RoomInfo();
                $roomInfo->setDateDetails(\DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-'.$month.'-'.$date))));
                $roomInfo->setCapacity(5);
                $roomInfo->setPrice(6);
                $roomInfo->setExistingAmount(5);
                $roomInfo->setCreatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
                $roomInfo->setUpdatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
                $manager->persist($roomInfo);
                $manager->flush();

            }

        }

        $reservation = new ReservationDetails();
        $reservation->setStartDate(\DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-14'))));
        $reservation->setEndDate(\DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-19'))));
        $reservation->setCreatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        $reservation->setUpdatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        $manager->persist($reservation);
        $manager->flush();

        $reservation = new ReservationDetails();
        $reservation->setStartDate(\DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-16'))));
        $reservation->setEndDate(\DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-16'))));
        $reservation->setCreatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        $reservation->setUpdatedAt(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        $manager->persist($reservation);
        $manager->flush();

        $roomInfo = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-14')))]);
        $roomInfo->setExistingAmount($roomInfo->getExistingAmount() - 1);
        $manager->persist($roomInfo);
        $manager->flush();

        $roomInfo = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-15')))]);
        $roomInfo->setExistingAmount($roomInfo->getExistingAmount() - 1);
        $manager->persist($roomInfo);
        $manager->flush();

        $roomInfo = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-16')))]);
        $roomInfo->setExistingAmount($roomInfo->getExistingAmount() - 1);
        $manager->persist($roomInfo);
        $manager->flush();

        $roomInfo = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-17')))]);
        $roomInfo->setExistingAmount($roomInfo->getExistingAmount() - 1);
        $manager->persist($roomInfo);
        $manager->flush();

        $roomInfo = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-18')))]);
        $roomInfo->setExistingAmount($roomInfo->getExistingAmount() - 1);
        $manager->persist($roomInfo);
        $manager->flush();

        $roomInfo = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-19')))]);
        $roomInfo->setExistingAmount($roomInfo->getExistingAmount() - 1);
        $manager->persist($roomInfo);
        $manager->flush();

        $roomInfo = $this->roomInfoRepository->findOneBy(['dateDetails' => \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('2021-07-16')))]);
        $roomInfo->setExistingAmount($roomInfo->getExistingAmount() - 1);
        $manager->persist($roomInfo);
        $manager->flush();
    }
}
